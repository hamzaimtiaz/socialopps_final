from facebook_business.api import FacebookAdsApi
from facebook_business.adobjects.adaccount import AdAccount
from facebook_business.adobjects.campaign import Campaign
from facebook_business.adobjects.targetingsearch import TargetingSearch
from facebook_business.adobjects.targeting import Targeting
from facebook_business.adobjects.adimage import AdImage
from facebook_business.adobjects.adset import AdSet
from facebook_business.adobjects.targeting import Targeting

from socialopps.settings import PAGE_ID, ACCESS_TOKEN, AD_ACCOUNT_ID, APP_SECRET, PAGE_LINK


class addPostFb():

    def __init__(self, special_feature, photo_url, unit_title, placement, budget, start_date, end_date, inventory_id):
        self.special_feature = special_feature
        self.photo_url = photo_url
        self.unit_title = unit_title
        # self.schedule_length = schedule_length
        self.placement = placement
        self.start_date = start_date
        self.end_date = end_date
        # self.fb_id = fb_id
        self.inventory_id = inventory_id
        self.page_id = PAGE_ID
        self.ad_account_id = AD_ACCOUNT_ID
        self.app_secret = APP_SECRET
        self.page_link = PAGE_LINK
        FacebookAdsApi.init(access_token=ACCESS_TOKEN)
        

    def compose_and_post(self):

        # Creating a campaign
        params = {
            'name': f"campaign for {self.unit_title}",
            'objective': objective,
            'status': 'PAUSED',
        }
        campaign_id = AdAccount(self.ad_account_id).create_campaign(params=params)
        print('campaign created with id',campaign_id)

        # Creation of AddSETS
        adset = AdSet(parent_id=self.ad_account_id)
        adset.update({
            AdSet.Field.name: f"Adset for {self.unit_title}",
            AdSet.Field.campaign_id: campaign_id["id"],
            # AdSet.Field.daily_budget: budget,
            AdSet.Field.lifetime_budget: budget,
            # AdSet.Field.start_time: start_time,
            # AdSet.Field.end_time: end_time,
            # AdSet.Field.billing_event: AdSet.BillingEvent.impressions,
            AdSet.Field.optimization_goal: AdSet.OptimizationGoal.reach,
            AdSet.Field.bid_amount: 10,
            AdSet.Field.targeting: {
                # placement value should be list = ['facebook','instagram','messenger','audience_network']
                Targeting.Field.publisher_platforms: self.placement,
                Targeting.Field.geo_locations: {
                    'countries': ad_country,
                },
            },
        })
        adset_id = adset.remote_create(params={
            'status': AdSet.Status.paused,
        })
         
        print('adset id is',adset_id)
        # Uploading of an Image
        image = AdImage(parent_id=self.ad_account_id)
        image[AdImage.Field.filename] = self.photo_url
        image.remote_create()
        image_hash = image[AdImage.Field.hash]

        # Creating the Ad
        fields = []
        params = {
            'name': f"{self.unit_title}_Creative",
            'object_story_spec': {'page_id':self.page_id,'link_data':{'image_hash':image_hash,'link':self.page_link,'message':self.special_feature}},}
        adcreative = AdAccount(self.ad_account_id).create_ad_creative(fields=fields, params=params)
        print('addcreative id:',adcreative)

        # Linking wit AD
        params = {
        'name': f"Ad for {self.unit_title}",
        'adset_id': adset["id"],
        'creative': {'creative_id':adcreative["id"]},
        'status': 'PAUSED',
        }
        add = AdAccount(self.ad_account_id).create_ad(params=params)

        print('ad id:', add)
