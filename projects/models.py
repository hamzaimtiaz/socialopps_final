from django.db import models


class customer(models.Model):
    name = models.CharField(max_length=50)
    email = models.EmailField(unique=True)
    phone = models.CharField(max_length=50)
    address = models.CharField(max_length=50)

class leads(models.Model):
    date = models.DateField()
    unit_of_interest = models.CharField(max_length=50)

class appointments(models.Model):
    date = models.DateField()
    time = models.CharField(max_length=50)
    notes = models.TextField()
    customer_id = models.ForeignKey(customer, on_delete=models.CASCADE)

class inventory(models.Model):
    stock_no = models.TextField()
    name = models.CharField(max_length=50)
    unit = models.TextField()
    days = models.IntegerField()
    selling_price  = models.IntegerField()
    special_feature = models.TextField()
    quantity = models.IntegerField()
    owner = models.ForeignKey('auth.User', related_name='inventory', on_delete=models.CASCADE) 
    class Meta:
        ordering = ('stock_no',)

    def __str__(self):
        return self.stock_no

class adposts(models.Model):
    special_feature = models.TextField()
    photo_url = models.TextField()
    unit_title = models.TextField()
    audience = models.TextField()
    schedule_length = models.TextField()
    placement   = models.TextField()
    budget = models.IntegerField()
    payment_details = models.TextField()
    time = models.IntegerField()
    start_date = models.DateField()
    end_date = models.DateField()
    fb_page_id = models.TextField()
    ad_status = models.BooleanField()
    inventory_id = models.ForeignKey(inventory, on_delete=models.CASCADE)
