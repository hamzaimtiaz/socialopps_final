from django.contrib import admin
from import_export.admin import ImportExportModelAdmin  
from .models import customer ,leads ,appointments ,inventory ,adposts

# Register your models here.
admin.site.register(customer)
# admin.site.register(leads)
admin.site.register(appointments)
# admin.site.register(inventory)
admin.site.register(adposts)

@admin.register(inventory,leads)
class ViewAdmin(ImportExportModelAdmin):
     pass