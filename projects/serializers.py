from rest_framework import serializers
from django.contrib.auth.models import User

from .models import inventory,customer,appointments, adposts

class inventorySerializer(serializers.ModelSerializer):

    class Meta:
        model = inventory
        fields = ('id','stock_no', 'name', 'unit', 'days',
                  'selling_price', 'special_feature','quantity' )
            
class UserSerializer(serializers.ModelSerializer):
    # inventory = serializers.PrimaryKeyRelatedField(
    #     many=True, queryset=inventory.objects.all())

    model = User
    def create(self, validated_data):
        password = validated_data.pop('password', None)
        instance = self.Meta.model(**validated_data)
        if password is not None:
            instance.set_password(password)
        instance.save()
        return instance

    class Meta:
        model = User
        fields = ("id","username","first_name","last_name","password","email")

class UserLogin(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username','password')

class TokenSerializer(serializers.Serializer):
    """
    This serializer serializes the token data
    """
    token = serializers.CharField(max_length=255)

class CustomerSerializer(serializers.ModelSerializer):

    class Meta:
        model = customer
        fields = ('name', 'email', 'phone', 'address')

class AppointmentsSerializer(serializers.ModelSerializer):

    class Meta:
        model = appointments
        fields = ('id','date', 'time', 'notes', 'customer_id')


class AdpostSerializer(serializers.ModelSerializer):
    class Meta:
        model = adposts
        fields = ('special_feature', 'photo_url', 'unit_title', 'audience',
                  'schedule_length', 'placement','budget','payment_details',
                  'time','start_date', 'end_date','fb_page_id','ad_status', 'inventory_id' )
