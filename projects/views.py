import datetime
from rest_framework import status
from rest_framework import generics, permissions
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from django.shortcuts import render
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User 
from drf_multiple_model.views import ObjectMultipleModelAPIView
from rest_framework_jwt.settings import api_settings
from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework import viewsets

from adPostFb import addPostFb
from .models import inventory, customer, appointments, adposts
from .serializers import inventorySerializer ,UserSerializer,TokenSerializer, \
                    AppointmentsSerializer,CustomerSerializer,AdpostSerializer


class inventoryList(generics.ListCreateAPIView,generics.DestroyAPIView):
    # permission_classes = (permissions.IsAuthenticated,)
    queryset = inventory.objects.all()
    serializer_class = inventorySerializer

    # def perform_create(self, serializer):
    #     serializer.save(owner=self.request.user)


class inventoryDetail(generics.RetrieveUpdateDestroyAPIView):
    # permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    queryset =inventory.objects.all()
    serializer_class = inventorySerializer


class UserList(generics.ListCreateAPIView, generics.DestroyAPIView): 
    
    # permission_classes = (permissions.AllowAny,)
    queryset = User.objects.all()
    serializer_class = UserSerializer
    

class UserDetail(generics.RetrieveUpdateDestroyAPIView): 
    
    # permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    queryset = User.objects.all()
    serializer_class = UserSerializer


class Customercard(generics.ListCreateAPIView):
    queryset = customer.objects.all()
    serializer_class = CustomerSerializer


class Appointments(generics.ListCreateAPIView):
    queryset = appointments.objects.all()
    serializer_class = AppointmentsSerializer

class AppointmentsDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = appointments.objects.all()
    serializer_class = AppointmentsSerializer



class inventoryAdpostList(APIView):
    serializer_class = AdpostSerializer

    def get(self, request, format=None):
        adpost = adposts.objects.all()
        serializer = AdpostSerializer(adpost, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = AdpostSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            audience_string = serializer.data["audience"]
            audience_list = audience_string.split
            placement_string = serializer.data["placement"]
            placement_list = placement_string.split
            start_date = serializer.data["start_date"]
            end_date = serializer.data["end_date"]
            addPostFb(
                serializer.data["special_feature"],
                serializer.data["photo_url"],
                serializer.data["unit_title"],
                # audience_list,
                # serializer.data["schedule_length"],
                placement_list,
                serializer.data["budget"],

                # serializer.data["payment_details"],
                # serializer.data["time"],
                start_date,
                end_date,
                # serializer.data["fb_page_id"],
                serializer.data["inventory_id"]
            )
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class activeAdposts(generics.ListAPIView):
    # queryset = adposts.objects.all()
    queryset = adposts.objects.filter(ad_status= True)
    serializer_class = AdpostSerializer

class inactiveAdposts(generics.ListAPIView):
    # queryset = adposts.objects.all()
    queryset = adposts.objects.filter(ad_status= False)
    serializer_class = AdpostSerializer