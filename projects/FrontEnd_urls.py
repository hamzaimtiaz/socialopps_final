from django.views.generic import TemplateView
from rest_framework.urlpatterns import format_suffix_patterns
from django.urls import path

urlpatterns = [
    ## Front End URL's
    path('inventory/', TemplateView.as_view(template_name='index.html')),
    path('users/', TemplateView.as_view(template_name='users.html')),
    path('appointments/', TemplateView.as_view(template_name='appointments.html')),
]

urlpatterns = format_suffix_patterns(urlpatterns)