from django.urls import path
from rest_framework_simplejwt import views as jwt_views
from rest_framework.urlpatterns import format_suffix_patterns

from projects import views
from rest_framework_simplejwt import views as jwt_views



urlpatterns = [
    path('token/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
    path('products/', views.inventoryList.as_view()),
    path('products/<int:pk>/', views.inventoryDetail.as_view()),
    path('users/', views.UserList.as_view()), 
    path('users/<int:pk>/', views.UserDetail.as_view()),
    path('appointments/', views.Appointments.as_view(), name='appointments'),
    path('appointments/<int:pk>/', views.AppointmentsDetail.as_view(), name='appointmentsDetail'),
    path('addpost/', views.inventoryAdpostList.as_view()),
    path('active_adposts/', views.activeAdposts.as_view()),
    path('inactive_adposts/', views.inactiveAdposts.as_view()),

]

urlpatterns = format_suffix_patterns(urlpatterns)
